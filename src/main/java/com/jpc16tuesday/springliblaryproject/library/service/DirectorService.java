package com.jpc16tuesday.springliblaryproject.library.service;
import com.jpc16tuesday.springliblaryproject.library.dto.DirectorDTO;
import com.jpc16tuesday.springliblaryproject.library.dto.FilmDTO;
import com.jpc16tuesday.springliblaryproject.library.mapper.DirectorMapper;
import com.jpc16tuesday.springliblaryproject.library.model.Director;
import com.jpc16tuesday.springliblaryproject.library.model.Film;
import com.jpc16tuesday.springliblaryproject.library.repository.DirectorRepository;
import com.jpc16tuesday.springliblaryproject.library.repository.FilmRepository;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;

@Service
public class DirectorService
        extends GenericService<Director, DirectorDTO> {

    private final FilmRepository filmRepository;


    public DirectorService(DirectorRepository repository,
                           DirectorMapper directorMapper,
                           FilmRepository filmRepository) {
        super(repository, directorMapper);
        this.filmRepository = filmRepository;
    }

    public DirectorDTO addFilm(Long filmId,
                             Long directorId) {
        Film film = filmRepository.findById(filmId).orElseThrow(() -> new NotFoundException("Фильм не найден!"));
        DirectorDTO director = getOne(directorId);
        director.getFilmIds().add(film.getId());
        update(director);
        return director;
    }
    public String getFilmsTitle(List<Long> ids){
        String str = "";
        Film film;
        for (Long id: ids){
            film = filmRepository.findById(id).orElseThrow();
            str =  str + "\n" + film.getFilmTitle();
        }
        if (str.length() > 1 ){
            str = str.substring(0, str.length() - 1);
        }
        if (str.length() == 0){
            str = "не указано";
        }
        return str;
    }
    public List<Film> getAllFilms(){
        return filmRepository.findAll();
    }
    public List<Film> getAllFilms(List<Long> ids){
        return filmRepository.findAllById(ids);
    }
}
