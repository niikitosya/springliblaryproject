package com.jpc16tuesday.springliblaryproject.library.service;

import com.jpc16tuesday.springliblaryproject.library.dto.OrdersDTO;
import com.jpc16tuesday.springliblaryproject.library.mapper.OrdersMapper;
import com.jpc16tuesday.springliblaryproject.library.model.Orders;
import com.jpc16tuesday.springliblaryproject.library.model.User;
import com.jpc16tuesday.springliblaryproject.library.repository.OrdersRepository;
import com.jpc16tuesday.springliblaryproject.library.repository.UserRepository;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrdersService
        extends GenericService<Orders, OrdersDTO> {
    private final UserRepository userRepository;
    protected OrdersService(OrdersRepository ordersRepository,
                            OrdersMapper ordersMapper,
                            UserRepository userRepository) {
        super(ordersRepository, ordersMapper);
        this.userRepository = userRepository;
    }
    public List<OrdersDTO> getAllRentedFilmByUser(Long userId){
        User user = userRepository.findById(userId).orElseThrow(() -> new NotFoundException("Пользователя с данным айди не существует"));
        return listAll().stream().filter((m) -> m.getUserId().equals(user.getId())).collect(Collectors.toList());
    }
}
