package com.jpc16tuesday.springliblaryproject.library.service;

import com.jpc16tuesday.springliblaryproject.library.dto.DirectorDTO;
import com.jpc16tuesday.springliblaryproject.library.dto.FilmDTO;
import com.jpc16tuesday.springliblaryproject.library.mapper.FilmMapper;
import com.jpc16tuesday.springliblaryproject.library.model.Director;
import com.jpc16tuesday.springliblaryproject.library.model.Film;
import com.jpc16tuesday.springliblaryproject.library.repository.DirectorRepository;
import com.jpc16tuesday.springliblaryproject.library.repository.FilmRepository;
import org.springframework.stereotype.Service;
import org.w3c.dom.stylesheets.LinkStyle;
import org.webjars.NotFoundException;

import java.util.List;

@Service
public class FilmService
        extends GenericService<Film, FilmDTO> {
    private final DirectorRepository directorRepository;

    protected FilmService(FilmRepository repository,
                          FilmMapper mapper,
                          DirectorRepository directorRepository) {
        super(repository, mapper);
        this.directorRepository = directorRepository;
    }

    public FilmDTO addDirector(final Long filmId,
                             final Long directorId) {
        FilmDTO film = getOne(filmId);
        Director director = directorRepository.findById(directorId).orElseThrow(() -> new NotFoundException("Режиссер не найден"));
        film.getDirectorIds().add(director.getId());
        update(film);
        return film;
    }
    public String getDirectorsFIO(List<Long> ids){
        String str = "";
        Director director;
        for (Long id:ids){
            director = directorRepository.findById(id).orElseThrow();
            str = str + "/n" + director.getDirectorFio();
        }
        if (str.length() > 1 ){
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }
}
