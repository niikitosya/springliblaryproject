package com.jpc16tuesday.springliblaryproject.library.mapper;

import com.jpc16tuesday.springliblaryproject.library.dto.OrdersDTO;
import com.jpc16tuesday.springliblaryproject.library.model.Orders;
import com.jpc16tuesday.springliblaryproject.library.repository.FilmRepository;
import com.jpc16tuesday.springliblaryproject.library.repository.UserRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;

import java.util.List;

@Component
public class OrdersMapper
        extends GenericMapper<Orders, OrdersDTO> {
    private final FilmRepository filmRepository;
    private final UserRepository userRepository;

    protected OrdersMapper(ModelMapper mapper,
                                 FilmRepository filmRepository,
                                 UserRepository userRepository) {
        super(Orders.class, OrdersDTO.class, mapper);
        this.filmRepository = filmRepository;
        this.userRepository = userRepository;
    }

    @PostConstruct
    public void setupMapper() {
        super.modelMapper.createTypeMap(Orders.class, OrdersDTO.class)
                .addMappings(m -> m.skip(OrdersDTO::setUserId))
                .addMappings(m -> m.skip(OrdersDTO::setFilmId))
                .setPostConverter(toDTOConverter());

        super.modelMapper.createTypeMap(OrdersDTO.class, Orders.class)
                .addMappings(m -> m.skip(Orders::setUser))
                .addMappings(m -> m.skip(Orders::setFilm))
                .setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(OrdersDTO source, Orders destination) {
        destination.setFilm(filmRepository.findById(source.getFilmId()).orElseThrow(() -> new NotFoundException("Фильма не найдено")));
        destination.setUser(userRepository.findById(source.getUserId()).orElseThrow(() -> new NotFoundException("Пользователя не найдено")));
    }

    @Override
    protected void mapSpecificFields(Orders source, OrdersDTO destination) {
        destination.setUserId(source.getUser().getId());
        destination.setFilmId(source.getFilm().getId());
    }

    @Override
    protected List<Long> getIds(Orders entity) {
        throw new UnsupportedOperationException("Метод недоступен");
    }
}
