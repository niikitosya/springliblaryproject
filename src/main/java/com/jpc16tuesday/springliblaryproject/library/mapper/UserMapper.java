package com.jpc16tuesday.springliblaryproject.library.mapper;

import com.jpc16tuesday.springliblaryproject.library.dto.UserDTO;
import com.jpc16tuesday.springliblaryproject.library.model.GenericModel;
import com.jpc16tuesday.springliblaryproject.library.model.User;
import com.jpc16tuesday.springliblaryproject.library.repository.OrdersRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class UserMapper
        extends GenericMapper<User, UserDTO> {

    private final OrdersRepository ordersRepository;

    protected UserMapper(ModelMapper modelMapper,
                         OrdersRepository ordersRepository) {
        super(User.class, UserDTO.class, modelMapper);
        this.ordersRepository = ordersRepository;
    }

    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(User.class, UserDTO.class)
                .addMappings(m -> m.skip(UserDTO::setOrders)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(UserDTO.class, User.class)
                .addMappings(m -> m.skip(User::setOrders)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(UserDTO source, User destination) {
        if (!Objects.isNull(source.getOrders())) {
            destination.setOrders(ordersRepository.findAllById(source.getOrders()));
        }
        else {
            destination.setOrders(Collections.emptyList());
        }
    }

    @Override
    protected void mapSpecificFields(User source, UserDTO destination) {
        destination.setOrders(getIds(source));
    }

    @Override
    protected List<Long> getIds(User entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getOrders())
                ? null
                : entity.getOrders().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toList());
    }
}
