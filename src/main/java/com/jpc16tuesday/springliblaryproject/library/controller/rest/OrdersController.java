package com.jpc16tuesday.springliblaryproject.library.controller.rest;

import com.jpc16tuesday.springliblaryproject.library.dto.OrdersDTO;
import com.jpc16tuesday.springliblaryproject.library.model.Orders;
import com.jpc16tuesday.springliblaryproject.library.service.OrdersService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/orders/info") // http://localhost:8081/swagger-ui/index.html#/orders/info
@Tag(name = "Прокат фильмов",
        description = "Контроллер для работы с прокатом фильмов пользователям сервиса")
public class OrdersController
        extends GenericController<Orders, OrdersDTO> {

    public OrdersController(OrdersService ordersService) {
        super(ordersService);
    }
    @Operation(description = "Показать все купленные/ арендованные фильмы пользователем")
    @RequestMapping(value = "/getUserFilms", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<OrdersDTO>> getAllUserRents(@RequestParam(value = "user_Id") Long userId){
        return ResponseEntity.status(HttpStatus.OK).body(((OrdersService) service).getAllRentedFilmByUser(userId));
    }
}