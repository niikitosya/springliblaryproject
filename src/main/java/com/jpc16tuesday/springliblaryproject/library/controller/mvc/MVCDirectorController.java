package com.jpc16tuesday.springliblaryproject.library.controller.mvc;


import com.jpc16tuesday.springliblaryproject.library.dto.DirectorDTO;
import com.jpc16tuesday.springliblaryproject.library.service.DirectorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@Controller
@RequestMapping("/directors")
public class MVCDirectorController {
    private final DirectorService directorService;

    public MVCDirectorController(DirectorService directorService) {

        this.directorService = directorService;
    }

    @GetMapping
    public String getAll(Model model) {
        List<DirectorDTO> directors = directorService.listAll();
        List<String> filmsTitles = directors.stream().map((e) -> directorService.getFilmsTitle(e.getFilmIds())).collect(Collectors.toList());
        model.addAttribute("directors", directors);
        model.addAttribute("filmsTitles", filmsTitles);
        log.info("kmknkn" + (directorService.getAllFilms().size()));
        model.addAttribute("AllFilms", directorService.getAllFilms());
        return "directors/viewAllDirectors";
    }
    @GetMapping("/add")
    public String create() {
        return "directors/addDirector";
    }

    @PostMapping("/add")
    public String create(@ModelAttribute("directorForm") DirectorDTO newDirector) {
        log.info(newDirector.toString());
        directorService.create(newDirector);
        return "redirect:/directors";
    }

    @GetMapping("/moderation/edit/{id}")
    public String editFilm(@PathVariable("id") Long id, Model model) {
        DirectorDTO director = directorService.getOne(id);
        model.addAttribute("directorForm", director);
        model.addAttribute("directorFilms", directorService.getAllFilms(director.getFilmIds()));
        log.info("kmknkn" + (directorService.getAllFilms().size()));
        model.addAttribute("AllFilms", directorService.getAllFilms());
        return "directors/updateDirector";
    }

    @GetMapping("/moderation")
    public String moderate(Model model) {
        List<DirectorDTO> directors = directorService.listAll();
        model.addAttribute("directors", directors);
        return "directors/ModerationPage";
    }

    @GetMapping("/remove/{id}")
    public String remove(@PathVariable("id") Long directorId) {
        log.info("На удаление" + directorService.getOne(directorId).toString());
        directorService.delete(directorService.getOne(directorId).getId());
        return "redirect:/directors/moderation";
    }
    @PostMapping("/update/{id}")
    public String updateFilm(@PathVariable("id") Long id, @ModelAttribute("directorForm") DirectorDTO directorForm,
                             @RequestParam(value = "selectedFilms", required = false) List<Long> selectedFilms,  @RequestParam( required = false, value = "removedFilms") List<Long> removedFilms){
        // Обработка данных формы и обновление фильма в репозитории
        DirectorDTO director = directorService.getOne(id);
        director.setDirectorFio(directorForm.getDirectorFio());
        director.setPosition(directorForm.getPosition());
        if (!Objects.isNull(selectedFilms)) director.getFilmIds().addAll(selectedFilms);
        if (!Objects.isNull(removedFilms)) director.getFilmIds().removeAll(removedFilms);
        directorService.update(director);
        return "redirect:/directors/moderation"; // Редирект на страницу со списком фильмов
    }
}
