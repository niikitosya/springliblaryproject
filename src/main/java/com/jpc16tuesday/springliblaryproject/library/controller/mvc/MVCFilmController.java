package com.jpc16tuesday.springliblaryproject.library.controller.mvc;

import com.jpc16tuesday.springliblaryproject.library.dto.FilmDTO;
import com.jpc16tuesday.springliblaryproject.library.model.Film;
import com.jpc16tuesday.springliblaryproject.library.service.DirectorService;
import com.jpc16tuesday.springliblaryproject.library.service.FilmService;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Controller
@RequestMapping("/films")
public class MVCFilmController {
    private final FilmService filmService;
    public MVCFilmController(FilmService filmService, DirectorService directorService) {
        this.filmService = filmService;
    }

    @GetMapping
    public String getAll(Model model) {
        List<FilmDTO> films = filmService.listAll();
        List<String> directorFIOs= films.stream().map((e) -> filmService.getDirectorsFIO(e.getDirectorIds())).collect(Collectors.toList());
        model.addAttribute("films", films);
        model.addAttribute("directorFIOs", directorFIOs);
        return "films/viewAllFilms";
    }

    @GetMapping("/add")
    public String create() {
        return "films/addFilm";
    }

    @PostMapping("/add")
    public String create(@ModelAttribute("filmForm") FilmDTO newFilm) {
        log.info(newFilm.toString());
        filmService.create(newFilm);
        return "redirect:/films";
    }
    @GetMapping("/moderation/edit/{id}")
    public String editFilm(@PathVariable("id") Long id, Model model) {
        FilmDTO film = filmService.getOne(id);
        model.addAttribute("filmForm", film);
        return "films/updateFilm";
    }

    @GetMapping("/moderation")
    public String moderate(Model model) {
        List<FilmDTO> films = filmService.listAll();
        model.addAttribute("films", films);

        return "films/ModerationPage";
    }

    @GetMapping("/remove/{id}")
    public String remove(@PathVariable("id") Long filmId) {
        log.info("На удаление" + filmService.getOne(filmId).toString());
        filmService.delete(filmService.getOne(filmId).getId());
        return "redirect:/moderation";
    }
    @PostMapping("/update/{id}")
    public String updateFilm(@PathVariable("id") Long id, @ModelAttribute("filmForm") FilmDTO filmForm) {
        // Обработка данных формы и обновление фильма в репозитории
        FilmDTO film = filmService.getOne(id);
        film.setFilmTitle(filmForm.getFilmTitle());
        film.setGenre(filmForm.getGenre());
        film.setPremiereDate(filmForm.getPremiereDate());
        film.setCountry(filmForm.getCountry());
        filmService.update(film);
        return "redirect:/films/moderation"; // Редирект на страницу со списком фильмов
    }
}
