package com.jpc16tuesday.springliblaryproject.library.dto;

import com.jpc16tuesday.springliblaryproject.library.model.Genre;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDate;
import java.util.List;

@ToString
@Getter
@Setter
@NoArgsConstructor
public class FilmDTO extends GenericDTO{
    private String filmTitle;
    private Genre genre;
    private LocalDate premiereDate;
    private String country;
    private List<Long> directorIds;
}
