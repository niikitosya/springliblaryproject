package com.jpc16tuesday.springliblaryproject.library.repository;

import com.jpc16tuesday.springliblaryproject.library.model.Director;
import org.springframework.stereotype.Repository;

@Repository
public interface DirectorRepository
        extends GenericRepository<Director> {
}
