package com.jpc16tuesday.springliblaryproject.library.repository;

import com.jpc16tuesday.springliblaryproject.library.model.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository
        extends GenericRepository<User> {
}
