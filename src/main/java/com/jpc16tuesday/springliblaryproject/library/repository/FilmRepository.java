package com.jpc16tuesday.springliblaryproject.library.repository;

import com.jpc16tuesday.springliblaryproject.library.model.Film;
import org.springframework.stereotype.Repository;

@Repository
public interface FilmRepository
        extends GenericRepository<Film> {
}
