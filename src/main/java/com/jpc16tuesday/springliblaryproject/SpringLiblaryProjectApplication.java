package com.jpc16tuesday.springliblaryproject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
@SpringBootApplication
public class SpringLiblaryProjectApplication implements CommandLineRunner {
    @Value("${server.servlet.context-path}")
    private String contextPath;
    @Value("${server.port}")
    private String port;
    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;
    public static void main(String[] args) {
        SpringApplication.run(SpringLiblaryProjectApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Swagger path: http://localhost:" + port + contextPath);
    }
}
